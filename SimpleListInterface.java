/**
 * Interface for simplified list.
 * 
 * @author Michael Siff
 */
public interface SimpleListInterface<T>
{

    /**
     * Returns number of elements on list.
     * @return number of elements on list
     */
    int size();
    
    /**
     * Returns element at specified position on list.
     * @param index position of element to return
     * @return element at specified position on list
     */
    T get(int index);
    
    /**
     * Replaces whatever was at specified position on list with
     * specified element.
     * @param index position of element to return
     * @param element to put into list
     */
    void set(int index, T element);
    
    /**
     * Appends new element onto back of list.
     * @param element to appended onto list
     */
    void append(T element);
    
    /**
     * Returns position of element in list if present, otherwise returns
     * -1.
     * @param element item to be searched for
     * @return position of element in list or -1 if not found
     */
    int indexOf(T element);

    /**
     * Returns string representation of list.
     * @return string representation of list
     */
    String toString();
    
}
